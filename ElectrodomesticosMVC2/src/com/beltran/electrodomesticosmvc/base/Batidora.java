package com.beltran.electrodomesticosmvc.base;

import java.time.LocalDate;

/**
 * Esta clase representa una batidora y herada de electrodomestico
 */
public class Batidora extends Electrodomestico {
    private int velocidades;

    /**
     * Constructor vacio de una batidora
     */
    public Batidora() {
        super();
    }

    /**
     * Constructor de una batidora con cinco parametros, cuatro compartidos con electrodomestico y uno añadido(velocidades)
     * @param codigoID Representa un codigo identificativo para cada batidora
     * @param marca Representa la marca de la batidora
     * @param modelo Representa el modelo de la batidora
     * @param fechaLanzamiento Representa la fecha de lanzamiento al mercado de la batidora
     * @param velocidades Representa el numero de velocidades en las que puede operar la batidora
     */
    public Batidora(String codigoID, String marca, String modelo, LocalDate fechaLanzamiento, int velocidades) {
        super(codigoID, marca, modelo, fechaLanzamiento);
        this.velocidades = velocidades;
    }

    /**
     *
     * @return Devuelve el valor del numero de velocidades en las que puede operar la batidora
     */
    public int getVelocidades() {
        return velocidades;
    }

    /**
     * Cambia el valor del numero de velocidades en las que puede operar la batidora
     * @param velocidades
     */
    public void setVelocidades(int velocidades) {
        this.velocidades = velocidades;
    }

    /**
     *
     * @return Metodo que devuelve una cadena de palabras con las caracrteristicas de cada batidora: codigo identificativo, marca, modelo, velocidades y fecha de lanzamiento
     */
    @Override
    public String toString() {
        return "Batidora:" +getCodigoID()+""+getMarca()+""+getModelo()+""+getVelocidades()+""+getFechaLanzamiento();
    }
}

