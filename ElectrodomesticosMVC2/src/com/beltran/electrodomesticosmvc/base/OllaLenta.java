package com.beltran.electrodomesticosmvc.base;

import java.time.LocalDate;

/**
 * Esta clase representa una olla lenta y herada de electrodomestico
 */
public class OllaLenta extends Electrodomestico {
    private int wattios;

    /**
     * Constructor vacio de una olla lenta
     */
    public OllaLenta() {
        super();
    }

    /**
     * Constructor de una olla lenta con cinco parametros, cuatro compartidos con electrodomestico y uno añadido(wattios)
     * @param codigoID Representa un codigo identificativo para cada olla lenta
     * @param marca Representa la marca de la olla lenta
     * @param modelo Representa el modelo de la olla lenta
     * @param fechaLanzamiento Representa la fecha de lanzamiento al mercado de la olla lenta
     * @param wattios Representa los wattios que consume la olla lenta cuando está encendida
     */
    public OllaLenta(String codigoID, String marca, String modelo, LocalDate fechaLanzamiento, int wattios) {
        super(codigoID, marca, modelo, fechaLanzamiento);
        this.wattios = wattios;
    }

    /**
     *
     * @return Devuelve los wattios que consume la olla lenta cuando está encendida
     */
    public int getWattios() {
        return wattios;
    }

    /**
     *
     * @param wattios Cambia los wattios que consume la olla lenta cuando está encendida
     */
    public void setWattios(int wattios) {
        this.wattios = wattios;
    }

    /**
     *
     * @return Metodo que devuelve una cadena de palabras con las caracrteristicas de cada olla lenta: codigo identificativo, marca, modelo, wattios y fecha de lanzamiento
     */
    @Override
    public String toString() {
        return "Olla Lenta:" +getCodigoID()+""+getMarca()+""+getModelo()+""+getWattios()+""+getFechaLanzamiento();
    }
}
