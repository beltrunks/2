package com.beltran.electrodomesticosmvc.base;

import java.time.LocalDate;

/**
 *Esta clase representa un electrodomestico
 */
public class Electrodomestico {
    private String codigoID;
    private String marca;
    private String modelo;
    private LocalDate fechaLanzamiento;

    /**
     *Constructor para un electrodomestico con 4 parametros
     * @param codigoID Representa un codigo identificativo para cada electrodomestico
     * @param marca Representa la marca del electrodomestico
     * @param modelo Representa el modelo del electrodomestico
     * @param fechaLanzamiento Representa la fecha de lanzamiento al mercado del electrodomestico
     */
    public Electrodomestico(String codigoID, String marca, String modelo, LocalDate fechaLanzamiento) {
        this.codigoID = codigoID;
        this.marca = marca;
        this.modelo = modelo;
        this.fechaLanzamiento = fechaLanzamiento;
    }

    /**
     * Constructor vacio para un electrodomestico
     */
    public Electrodomestico() {
    }

    /**
     *
     * @return Devuelve el valor del codigo identificativo del electrodomestico
     */
    public String getCodigoID() {
        return codigoID;
    }

    /**
     *
     * @param codigoID Cambia el valor del codigo identificativo del electrodomestico
     */

    public void setCodigoID(String codigoID) {
        this.codigoID = codigoID;
    }

    /**
     *
     * @return Devuelve la marca del electrodomestico
     */
    public String getMarca() {
        return marca;
    }

    /**
     *
     * @param marca Cambia el valor de la marca del electrodomestico
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     *
     * @return Devuelve el modelo del electrodomestico
     */
    public String getModelo() {
        return modelo;
    }

    /**
     *
     * @param modelo Cambia el valor de el modelo del electrodomestico
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     *
     * @return Devuelve la fecha de lanzamiento al mercado del electrodomestico
     */
    public LocalDate getFechaLanzamiento() {
        return fechaLanzamiento;
    }

    /**
     *
     * @param fechaLanzamiento Cambia el valor de la fecha de lanzamiento al mercado del electrodomestico
     */
    public void setFechaLanzamiento(LocalDate fechaLanzamiento) {
        this.fechaLanzamiento = fechaLanzamiento;
    }
}
