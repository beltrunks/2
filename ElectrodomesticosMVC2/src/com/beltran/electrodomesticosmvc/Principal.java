package com.beltran.electrodomesticosmvc;

import com.beltran.electrodomesticosmvc.gui.ElectrodomesticosControlador;
import com.beltran.electrodomesticosmvc.gui.ElectrodomesticosModelo;
import com.beltran.electrodomesticosmvc.gui.Ventana;

public class Principal {
    public static void main(String[] args) {
        Ventana vista = new Ventana();
        ElectrodomesticosModelo modelo = new ElectrodomesticosModelo();
        ElectrodomesticosControlador controlador = new ElectrodomesticosControlador(vista, modelo);
    }
}
