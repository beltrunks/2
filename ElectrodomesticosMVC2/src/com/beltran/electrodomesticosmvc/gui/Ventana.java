package com.beltran.electrodomesticosmvc.gui;

import com.beltran.electrodomesticosmvc.base.Electrodomestico;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;


public class Ventana {
    private JPanel panel1;
    public JFrame frame;
    public JRadioButton batidoraRadioButton;
    public JRadioButton ollaLentaRadioButton;
    public JTextField codigoIDTxt;
    public JTextField marcaTxt;
    public JTextField modeloTxt;
    public JTextField velocidadesWatiosTxt;
    public JLabel CodigoID;
    public JLabel Marca;
    public JLabel Modelo;
    public JLabel FechaLanzamiento;
    public JLabel VelocidadesWattios;
    public JButton nuevoButton;
    public DatePicker fechaLanzamientoDPicker;
    public JList list1;
    public JButton exportarButton;
    public JButton importarButton;
    public JButton limpiarTodoButton;
    public JButton borrarUltimoButton;

    //Elementos creados por mi
    public DefaultListModel<Electrodomestico> dlmElectrodomestico;

    public Ventana() {
        frame = new JFrame("Electrodomesticos MVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    private void initComponents(){
        dlmElectrodomestico = new DefaultListModel<Electrodomestico>();
        list1.setModel(dlmElectrodomestico);
    }
}
