package com.beltran.electrodomesticosmvc.gui;

import com.beltran.electrodomesticosmvc.gui.Ventana;
import com.beltran.electrodomesticosmvc.base.Batidora;
import com.beltran.electrodomesticosmvc.base.Electrodomestico;
import com.beltran.electrodomesticosmvc.base.OllaLenta;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ElectrodomesticosModelo {
    private ArrayList<Electrodomestico> listaElectrodomesticos;

    public ElectrodomesticosModelo() {
        listaElectrodomesticos = new ArrayList<Electrodomestico>();
    }

    public ArrayList<Electrodomestico> obtenerElectrodomesticos(){
        return listaElectrodomesticos;
    }

    public void borrarLista() {
        listaElectrodomesticos.clear();
    }

    public void borrarUltimo() {
        listaElectrodomesticos.remove(listaElectrodomesticos.size()-1);
    }

    public void altaBatidora (String codigoID, String marca, String modelo, LocalDate fechaLanzamiento, int velocidades) {
        Batidora nuevaBatidora = new Batidora(codigoID, marca, modelo, fechaLanzamiento, velocidades);
        listaElectrodomesticos.add(nuevaBatidora);
    }

    public void altaOllaLenta (String codigoID, String marca, String modelo, LocalDate fechaLanzamiento, int wattios) {
        OllaLenta nuevaOllaLenta = new OllaLenta(codigoID, marca, modelo, fechaLanzamiento, wattios);
        listaElectrodomesticos.add(nuevaOllaLenta);
    }

    public boolean existeCodigoID(String codigoID){
        for (Electrodomestico unElectrodomestico:listaElectrodomesticos) {
            if (unElectrodomestico.getCodigoID().equals(codigoID)) {
                return true;
            }
        }
        return false;
    }

    public void exportarXML (File fichero) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        Element raiz = documento.createElement("Electrodomesticos");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoElectrodomestico = null, nodoDatos = null;
        Text texto = null;

        for (Electrodomestico unElectrodomestico : listaElectrodomesticos) {

            /*Añado dentro de la etiqueta raiz (Electrodomesticos) una etiqueta
            dependiendo del tipo de vehiculo que este almacenando
            (batidora u olla lenta)
             */
            if (unElectrodomestico instanceof Batidora) {
                nodoElectrodomestico = documento.createElement("Batidora");

            } else {
                nodoElectrodomestico = documento.createElement("Olla-Lenta");
            }
            raiz.appendChild(nodoElectrodomestico);

            /*Dentro de la etiqueta electrodomestico le añado
            las subetiquetas con los datos de sus
            atributos (codigoID, marca, etc) (String codigoID, String marca, String modelo, LocalDate fechaLanzamiento)
             */
            nodoDatos = documento.createElement("codigoID");
            nodoElectrodomestico.appendChild(nodoDatos);

            texto = documento.createTextNode(unElectrodomestico.getCodigoID());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("marca");
            nodoElectrodomestico.appendChild(nodoDatos);

            texto = documento.createTextNode(unElectrodomestico.getMarca());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("modelo");
            nodoElectrodomestico.appendChild(nodoDatos);

            texto = documento.createTextNode(unElectrodomestico.getModelo());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("fecha-lanzamiento");
            nodoElectrodomestico.appendChild(nodoDatos);

            texto = documento.createTextNode(unElectrodomestico.getFechaLanzamiento().toString());
            nodoDatos.appendChild(texto);

            /* Como hay un dato que depende del tipo de electrodomestico
            debo acceder a él controlando el tipo de objeto
             */

            if (unElectrodomestico instanceof Batidora) {
                nodoDatos = documento.createElement("numero-velocidades");
                nodoElectrodomestico.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Batidora) unElectrodomestico).getVelocidades()));
                nodoDatos.appendChild(texto);
            } else {
                nodoDatos = documento.createElement("wattios");
                nodoElectrodomestico.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((OllaLenta) unElectrodomestico).getWattios()));
                nodoDatos.appendChild(texto);
            }
        }

            /*
            Guardo los datos en "fichero" que es el objeto File
            recibido por parametro
            */

        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);
    }

    public void importarXML (File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaElectrodomesticos = new ArrayList<Electrodomestico>();
        Batidora nuevaBatidora = null;
        OllaLenta nuevaOllaLenta = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength() ; i++) {
            Element nodoElectrodomestico = (Element) listaElementos.item(i);

            if (nodoElectrodomestico.getTagName().equals("Batidora")) {
                nuevaBatidora = new Batidora();
                nuevaBatidora.setCodigoID(nodoElectrodomestico.getChildNodes().item(0).getTextContent());
                nuevaBatidora.setMarca(nodoElectrodomestico.getChildNodes().item(1).getTextContent());
                nuevaBatidora.setModelo(nodoElectrodomestico.getChildNodes().item(2).getTextContent());
                nuevaBatidora.setFechaLanzamiento(LocalDate.parse(nodoElectrodomestico.getChildNodes().item(3).getTextContent()));
                nuevaBatidora.setVelocidades(Integer.parseInt(nodoElectrodomestico.getChildNodes().item(4).getTextContent()));

                listaElectrodomesticos.add(nuevaBatidora);
            } else {
                if (nodoElectrodomestico.getTagName().equals("Olla-Lenta")) {
                    nuevaOllaLenta = new OllaLenta();
                    nuevaOllaLenta.setCodigoID(nodoElectrodomestico.getChildNodes().item(0).getTextContent());
                    nuevaOllaLenta.setMarca(nodoElectrodomestico.getChildNodes().item(1).getTextContent());
                    nuevaOllaLenta.setModelo(nodoElectrodomestico.getChildNodes().item(2).getTextContent());
                    nuevaOllaLenta.setFechaLanzamiento(LocalDate.parse(nodoElectrodomestico.getChildNodes().item(3).getTextContent()));
                    nuevaOllaLenta.setWattios(Integer.parseInt(nodoElectrodomestico.getChildNodes().item(4).getTextContent()));

                    listaElectrodomesticos.add(nuevaOllaLenta);
                }
            }

        }




    }






































}

