package com.beltran.electrodomesticosmvc.gui;

import com.beltran.electrodomesticosmvc.base.Batidora;
import com.beltran.electrodomesticosmvc.base.Electrodomestico;
import com.beltran.electrodomesticosmvc.base.OllaLenta;
import com.beltran.electrodomesticosmvc.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

public class ElectrodomesticosControlador implements ActionListener, ListSelectionListener, WindowListener{
    private Ventana vista;
    private ElectrodomesticosModelo modelo;
    private File ultimaRutaExportada;

    public ElectrodomesticosControlador (Ventana vista, ElectrodomesticosModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);

    }

    private boolean hayCamposVacios(){
        if (vista.velocidadesWatiosTxt.getText().isEmpty() ||
                vista.marcaTxt.getText().isEmpty() ||
                vista.codigoIDTxt.getText().isEmpty() ||
                vista.modeloTxt.getText().isEmpty() ||
                vista.fechaLanzamientoDPicker.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    private void limpiarCampos() {
        vista.velocidadesWatiosTxt.setText(null);
        vista.marcaTxt.setText(null);
        vista.codigoIDTxt.setText(null);
        vista.modeloTxt.setText(null);
        vista.fechaLanzamientoDPicker.setText(null);
        vista.codigoIDTxt.setText(null);
    }

    private void refrescar() {
        vista.dlmElectrodomestico.clear();;
        for (Electrodomestico unElectrodomestico : modelo.obtenerElectrodomesticos()) {
            vista.dlmElectrodomestico.addElement(unElectrodomestico);
        }
    }

    private void addActionListener(ActionListener listener) {
        vista.batidoraRadioButton.addActionListener(listener);
        vista.ollaLentaRadioButton.addActionListener(listener);
        vista.nuevoButton.addActionListener(listener);
        vista.importarButton.addActionListener(listener);
        vista.exportarButton.addActionListener(listener);
        vista.limpiarTodoButton.addActionListener(listener);
        vista.borrarUltimoButton.addActionListener(listener);

    }

    private void addWindowListener (WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    private void addListSelectionListener(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }

    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("electrodomesticos.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    private void guardarConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("electrodomesticos.conf"), "Datos configuracion electrodomesticos");

    }


    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch (actionCommand) {
            case "Nuevo":
                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden e4star vacios\n" +
                            "CodigoID\nMarca\nModelo\nFecha" +
                            vista.velocidadesWatiosTxt);
                    break;
                }

                if (modelo.existeCodigoID(vista.codigoIDTxt.getText())) {
                    Util.mensajeError("Ya existe un electrodomestico con ese CodigoID\n+" +
                            vista.codigoIDTxt.getText());
                    break;
                }
                if (vista.batidoraRadioButton.isSelected()) {
                    modelo.altaBatidora(vista.codigoIDTxt.getText(),vista.marcaTxt.getText(),
                            vista.modeloTxt.getText(), vista.fechaLanzamientoDPicker.getDate(), Integer.parseInt(vista.velocidadesWatiosTxt.getText()));
                } else {
                    modelo.altaOllaLenta(vista.codigoIDTxt.getText(),vista.marcaTxt.getText(),
                            vista.modeloTxt.getText(), vista.fechaLanzamientoDPicker.getDate(), Integer.parseInt(vista.velocidadesWatiosTxt.getText()));
                }
                limpiarCampos();
                refrescar();
                break;
            case "Importar":
                JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;
            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada,"Archivo XML", "xml");
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML((selectorFichero2.getSelectedFile()));
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            case "Limpiar todo":
                modelo.borrarLista();
                refrescar();
                break;
            case "Borrar ultimo":
                modelo.borrarUltimo();
                refrescar();
                break;

            case "Batidora":
                vista.VelocidadesWattios.setText("Velocidades");
                break;
            case "Olla-Lenta":
                vista.VelocidadesWattios.setText("Wattios");
                break;

        }

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        int resp = Util.mensajeConfirmacion("¿Desea salir de la ventana?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                guardarConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            Electrodomestico electrodomesticoSeleccionado = (Electrodomestico) vista.list1.getSelectedValue();
            vista.codigoIDTxt.setText((electrodomesticoSeleccionado.getCodigoID()));
            vista.marcaTxt.setText(electrodomesticoSeleccionado.getMarca());
            vista.modeloTxt.setText(electrodomesticoSeleccionado.getModelo());
            vista.fechaLanzamientoDPicker.setDate(electrodomesticoSeleccionado.getFechaLanzamiento());

            if (electrodomesticoSeleccionado instanceof Batidora) {
                vista.batidoraRadioButton.doClick();
                vista.velocidadesWatiosTxt.setText(String.valueOf(((Batidora) electrodomesticoSeleccionado).getVelocidades()));
            } else {
                vista.ollaLentaRadioButton.doClick();
                vista.velocidadesWatiosTxt.setText(String.valueOf(((OllaLenta) electrodomesticoSeleccionado).getWattios()));
            }
        }
    }


























}
